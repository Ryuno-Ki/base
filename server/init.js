var fs = require('fs');
var path = require('path');
var debug = require('debug')('helpers');
var Templates = require('../main/server/templates/templates.js');

class BaseServer {

    constructor() {
        this.modules = [];
    }

    init() {
        debug('Base loading,...');

    }

    initModule(mod) {
        this.modules.push(mod);

        // Register any templates.
        if (mod.templates) {
            mod.templates.forEach((element) => {
                debug('Register ', element);
                Templates.registerModuleTemplate(mod.moduleId, element);
            })
        }

        if (mod.init) {
            mod.init();
        }
    }

    async registerModule(modulePath) {
        var configFile = path.join(modulePath, 'server','server.mjs');
        if (fs.existsSync(configFile)) {
            debug('Register module ' + modulePath);
            await import(configFile).then(mod => {
                this.initModule(mod.server);
            });
        } else {
            debug('Tried to register ' + modulePath + ' and failed (no server/config.mjs)');
        }
    }

    async postLoadModules() {
        debug('Loading all default templates.');
        await Templates.loadDefault();
    }

    async registerModuleList(modList) {
        await Promise.all(modList.map(async (modPath) => {
            await this.registerModule(modPath);
        }));
    }
}

module.exports = {
    BaseServer: BaseServer
}
