/**********************************************************
 * Initialize App - don't change anything from here!
 **********************************************************/
import axios from 'axios'
import Vue from 'vue'
import CountryFlag from 'vue-country-flag'
import VueI18n from 'vue-i18n'
import VueRouter from 'vue-router'

import AppConfig from './app.config.js'
import App from './App.vue'
import Consts from './consts.js'
import modMain from 'Main/config'
import store from 'Main/js/store.js'
import tokenHelper from 'Main/js/token-helper.js'
import validator from 'Main/js/validator.js'

// Load config
Vue.prototype.$config = AppConfig

// Import constants
Vue.prototype.$consts = Consts

// Configure vue router
Vue.use(VueRouter)

let routes = modMain.routes
for (const mod of AppConfig.allModules) {
  if (mod.routes) routes = routes.concat(mod.routes)
}
routes.push({ path: '/:ticket', name: 'main-notfound', component: () => import('Main/views/ShortLink.vue') }) // default route
const router = new VueRouter({ routes })

// Prepare calls to backend
axios.defaults.baseURL = AppConfig.urlBackend
Vue.prototype.$axios = axios

// Intercept request and insert Authorization (accessToken)
axios.interceptors.request.use(
  async config => {
    const accessToken = store.user.accessToken
    config.headers.Authorization = 'Bearer ' + accessToken
    return config
  },
  error => {
    Promise.reject(error)
  }
)

// Intercept answers and check for error codes
axios.interceptors.response.use((response) => { return response }, async function (error) {
  const originalRequest = error.config
  // handle error not-authorized
  if (error.response.status === 401 && !originalRequest._retry) {
    // remove invalid accessToken
    store.user.accessToken = ''
    // avoid loop on 401 errors
    originalRequest._retry = true
    // try to refresh token
    const accessToken = await tokenHelper.refreshAccessToken(axios)
    // failed to refresh token
    if (!accessToken) {
      store.user.clear()
      const queryParms = {}
      if (router.currentRoute.path !== undefined && router.currentRoute.path !== null) {
        queryParms.redirect = router.currentRoute.path
      }
      return router.push({ name: 'main-login', query: queryParms })
    }
    // successfully refreshed token
    store.user.accessToken = accessToken
    originalRequest.Authorization = 'Bearer ' + accessToken
    // resend original request with new token
    return axios(originalRequest)
  }
  // handle other errors
  if (error.response.status == 403) router.push({ name: 'main-forbidden' })
  if (error.response.status == 500) router.push({ name: 'main-error' })
  return Promise.reject(error)
})

// Create VueI18n instance with options
Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: AppConfig.defaultLocale,
  silentFallbackWarn: true
})

Vue.component('country-flag', CountryFlag)

// Validation
Vue.prototype.$v = validator.validate

// Start app
const app = new Vue({
  el: '#app',
  render: h => h(App),
  router,
  i18n
})
