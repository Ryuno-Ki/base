const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')
const { CleanWebpackPlugin } = require('clean-webpack-plugin') // installed via npm

module.exports = merge(common, {
  mode: 'production',
  watch: false,
  plugins: [new CleanWebpackPlugin()]
})
