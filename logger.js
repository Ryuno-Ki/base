exports.getLogger = function getLogger (prefix) {
	return {
		debug: console.debug.bind(console, prefix + ' %s'),
		error: console.error.bind(console, prefix + '%s'),
		log: console.log.bind(console, prefix + ' %s'),
		warn: console.warn.bind(console, prefix + ' %s'),
	};
}
