#! /usr/bin/env node

const fs = require('node:fs');
const configFile = "/srv/base/server.config.production.json";

//Create config file in case it does not exist yet
if (fs.existsSync(configFile)) {
    console.log("server.config.production.json file already exists and was not created.");
    process.exit(0)
} else {
    console.log("No server.config.production.json file found.");

    //Load example config into file
    var config = JSON.parse(fs.readFileSync('/srv/base/server.config.example.json'));

    //Fill in environment variables into the config file if they are set. If not, default values are used
    config.SITE_NAME = process.env.SITE_NAME || 'Lerntools';
    config.AUTHOR = process.env.AUTHOR || 'Lerntools Team';
    config.URL_PROTO = process.env.URL_PROTO || 'http://' ;
    config.URL_HOST = process.env.URL_HOST || 'localhost' ;
    config.URL_PREFIX = process.env.URL_PREFIX || '' ;
    config.INFO_MAIL = process.env.LERNTOOLS_MAIL || '' ;
    config.DEFAULT_LOCALE = process.env.DEFAULT_LOCALE || 'de' ;
    if(process.env.MONGO_URL){
        config.MONGO_URL = process.env.MONGO_URL;
    } else {
        console.log("URL for MongoDB is required. Provide it by using the environment variable MONGO_URL.");
        process.exit(1)
    }
    config.API_URL = process.env.API_URL || '/' ;
    config.USER_ADMIN_MAIL = process.env.USER_ADMIN_MAIL || '' ;
    config.TAN_EXPIRES = Number(process.env.TAN_EXPIRES) || 300 ;
    config.TAN_IGNORE = (process.env.TAN_IGNORE === "true") || false ;
    config.JWT.SECRET = process.env.SECRET || 'ENTER_A_VERY_GOOD_AND_RANDOM_SECRET_HERE' ;
    config.JWT.REFRESH_EXPIRES = Number(process.env.REFRESH_EXPIRES) || 36000 ;
    config.JWT.ACCESS_EXPIRES = Number(process.env.ACCESS_EXPIRES) || 600 ;
    config.JWT.RESTRICT_IP = (process.env.RESTRICT_IP === "true") || true ;
    config.REGISTRATION.ALLOWED = (process.env.REGISTRATION_ALLOWED === "true") || true ;
    config.REGISTRATION.MAX_USERS = Number(process.env.MAX_USERS) || 100 ;
    config.REGISTRATION.USER_SOFT_WARN = Number(process.env.USER_SOFT_WARN) || 15 ;
    config.REGISTRATION.USER_HARD_WARN = Number(process.env.USER_HARD_WARN) || 5 ;
    if(process.env.DEFAULT_ROLES){
        config.REGISTRATION.DEFAULT_ROLES = (process.env.DEFAULT_ROLES).split(' ') ;
    } else {
        config.REGISTRATION.DEFAULT_ROLES = ["ideas","chat"] ;
    }
    config.ACCOUNT_CLEANUP.ENABLED = (process.env.CLEANUP_ENABLED === "true") || true ;
    config.ACCOUNT_CLEANUP.EXPIRE_PERIOD = Number(process.env.CLEANUP_EXPIRE_PERIOD) || 300 ;
    config.ACCOUNT_CLEANUP.GRACE_WEEKS = Number(process.env.CLEANUP_GRACE_WEEKS) || 3 ;
    config.SMTP_CONF.host = process.env.SMTP_HOST || '' ;
    config.SMTP_CONF.port = Number(process.env.SMTP_PORT) || '' ;
    config.SMTP_CONF.auth.user = process.env.SMTP_USERNAME || '' ;
    config.SMTP_CONF.auth.pass = process.env.SMTP_PASSWORD || '' ;
    
    //Save the config as a file
    fs.writeFileSync(configFile, JSON.stringify(config, null, 2));

    console.log('Created config file from environment variables.');
}
