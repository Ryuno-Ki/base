#!/usr/bin/env python3
import glob
import logging
import os
import sys
import json

LOG = logging.getLogger(__name__)
log_format = logging.Formatter('[%(asctime)s] [%(levelname)s] - %(message)s')
LOG.setLevel(logging.INFO)

# writing to stdout
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
handler.setFormatter(log_format)
LOG.addHandler(handler)

def prepareLocalesFolder(localeFile):
    folder = os.path.dirname(localeFile)
    try:
        os.makedirs(folder, exist_ok=True)
    except Exception as e:
        LOG.error(f'Could not create folder {folder} ({str(e)})')

def convertFile(f):
    baseFileName = os.path.basename(f)
    localeFileName = os.path.join('locales', baseFileName.replace('.vue', '.json'))
    srcLocaleFile = os.path.join('..', localeFileName)
    LOG.info(f'Parsing {f} (base: {baseFileName} to locale: {localeFileName}')
    # extract the i18n
    cnt = open(f, 'rb').read()
    try:
        i18nStr = cnt.index(b'<i18n>')
    except ValueError:
        LOG.info(f'File {f} does not contain i18n')
        return None
    i18nEnd = cnt.index(b'</i18n>')
    localeData = cnt[i18nStr:i18nEnd+7]
    replaceStr = f'<i18n src="{srcLocaleFile}"/>'.encode('utf-8')
    cnt = cnt[:i18nStr] + replaceStr + cnt[i18nEnd+7:]
    with open(f, 'wb') as srcFile:
        srcFile.write(cnt)
    LOG.debug(f'Found locale data in {f}')
    prepareLocalesFolder(localeFileName)
    localeData = json.loads(localeData[6:-7].decode('utf-8'))
    with open(localeFileName, 'wb') as localeFile:
        localeFile.write(json.dumps(localeData, indent=4, ensure_ascii=False).encode('utf-8'))
    LOG.info(f'Updated {f} and wrote locale file {localeFileName}')

if __name__ == '__main__':
    for f in glob.glob('**/*.vue'):
        convertFile(f)