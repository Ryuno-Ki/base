% Title: Kontoregistrierung

Hallo {{user.login}},

auf der Seite {{instanceUrl}} wurde ein neues Konto für diese E-Mail-Adresse beantragt.

Bitte klicke auf den folgenden Link, um den Vorgang abzuschließen:
[{{confirmUrl}}]({{confirmUrl}})

Wenn Du diese Anfrage nicht selbst gestellt hast, ignoriere bitte diese E-Mail.

Dein Lerntools-Team
