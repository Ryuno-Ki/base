% Title: Code de transaction

Bonjour {{user.name}},

Le code pour confirmer ton action est : {{tan}}.

Si tu n'as pas lancé cette demande toi-même, tu peux ignorer cet e-mail.

Ton équipe Lerntools
