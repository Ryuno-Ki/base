% Title: Compte supprimé

Bonjour {{user.name}},

Ton compte avec toutes les données d'utilisateur·trice a maintenant été supprimé par [{{config.SITE_NAME}}]({{instanceUrl}}).

Ce processus ne peut pas être annulé. Tu peux créer un nouveau compte à tout moment.

Ton équipe Lerntools