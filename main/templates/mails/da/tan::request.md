% Title: Transaktionskode

Hej {{user.name}},

der Code zur Bestätigung Deiner Aktion lautet: {{tan}}.

Hvis du ikke selv har fremsat denne anmodning, skal du ignorere denne e-mail.

Dit team for læringsværktøjer
