% Title: Registrering af konto

hej {{user.login}},

auf der Seite {{instanceUrl}} wurde ein neues Konto für diese E-Mail-Adresse beantragt.

Bitte klicken Sie auf den folgenden Link, um den Vorgang abzuschließen:
[{{confirmUrl}}]({{confirmUrl}})

Hvis du ikke selv har fremsat denne anmodning, skal du ignorere denne e-mail.

Dit team for læringsværktøjer
