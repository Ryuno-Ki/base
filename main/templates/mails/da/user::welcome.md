% Title: Velkommen til {{config.SITE_NAME}}

Hej {{user.name}},

Velkommen til {{config.SITE_NAME}}. Din konto er nu klar.

Gå til [{{{instanceUrl}}}]({{instanceUrl}}) og kom i gang!

Dit team for læringsværktøjer