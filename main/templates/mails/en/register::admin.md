% Title: Register Account

A new user has registered on {{instanceUrl}}:

Name: {{user.name}}
E-mail: {{user.email}}
Comment: {{user.comment}}
