% Title: Password Reset

Hello {{user.name}},

A password reset was initiated for your account on page [{{instanceUrl}}]({{instanceUrl}}).

Please click the following link to confirm and reset:
[{{confirmUrl}}]({{confirmUrl}})

If you did not initiate this request yourself, please ignore this e-mail.
