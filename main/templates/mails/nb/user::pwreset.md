% Title: Passordtilbakestilling

Hei {{user.name}},

Passordtilbakestilling ble forespurt for din konto på [{{config.SITE_NAME}}]({{instanceUrl}}).

Klikk på lenken for å bekrefte og tilbakestille:
[{{confirmUrl}}]({{confirmUrl}})

Hvis du ikke tok initiativ til denne forespørselen kan du se bort fra denne e-posten.
