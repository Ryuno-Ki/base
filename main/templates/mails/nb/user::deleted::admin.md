% Title: Konto slettet

Kontoen din på [{{config.SITE_NAME}}]({{instanceUrl}}) har blitt slettet som følge av inaktivitet:

Navn: {{user.name}}
E-postadresse: {{user.email}}
Siste pålogging: {{user.lastLogin}}
