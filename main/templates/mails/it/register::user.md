% Title: Registrazione del conto

Ciao {{user.login}},

auf der Seite {{instanceUrl}} wurde ein neues Konto für diese E-Mail-Adresse beantragt.

Bitte klicke auf den folgenden Link, um den Vorgang abzuschließen:
[{{confirmUrl}}]({{confirmUrl}})

Se non avete avviato voi stessi questa richiesta, ignorate questa e-mail.

Il vostro team di strumenti di apprendimento
