import store from './store.js'
import consts from '../../consts.js'

export default {
  setLanguageByNavigator: function(ref) {
    var userLang = navigator.language || navigator.userLanguage;
    userLang = userLang.toLowerCase();
    var newLang = null;

    for (const key in consts.SUPPORTED_LANGUAGES) {
      for (const code in consts.SUPPORTED_LANGUAGES[key]) {
        if (code == userLang ||
            consts.SUPPORTED_LANGUAGES[key][code].alias.includes(userLang)) {
          newLang = code;
          break;
        }
      }
      if (newLang !== null) {
        break;
      }
    }
    if (newLang !== null) {
      this.updateLanguage(ref, newLang);
    }
  },
  updateLanguage: function (ref, lang) {
    // TODO: Perhaps worth to check against a list of known lang values?
    if (lang) {
      store.lang = lang
      ref.$i18n.locale = lang
      ref.$root.$i18n.locale = lang
      document.documentElement.setAttribute('lang', lang)
      console.debug('Changed language to ' + lang)
    } else {
      console.warn('Invalid language requested: ' + lang)
    }
  },
  updateUserProfile: function (ref, userData) {
    store.user.name = userData.name
    store.user.login = userData.login
    store.user.email = userData.email
    this.updateLanguage(ref, userData.lang)
  }
}
