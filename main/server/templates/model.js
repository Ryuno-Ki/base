var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var TemplateSchema=new Schema({
    type:		{type: String, required: true, max: 50},
    language:   {type: String, required: true, max: 3},
	title:		{type: String, required: true, max: 100},
	content:	{type: String, required: true, max: 1000},
    defaultTpl: {type: Boolean, required: true, default: false},
    updatedAt:  {type: Date, default: Date.now}
});
//middle ware in serial
TemplateSchema.pre('save', function preSave(next){
    var tpl = this;
    tpl.updatedAt = Date.now();
    next();
  });
  

module.exports=mongoose.model('Template', TemplateSchema);
