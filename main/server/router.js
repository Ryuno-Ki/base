var express=require('express');
var router=express.Router();
var controller=require('./controller');
var controllerTemplates=require('./templates/controller');


//link shortener
router.put('/shortlink', controller.checkIsUser, controller.shortenLink);
router.get('/shortlink/:ticket', controller.resolveLink);

//api - user
router.post('/login', controller.doLogin);
router.post('/token', controller.newAccessToken);
router.post('/logout', controller.doLogout);
router.get('/profile', controller.checkIsUser, controller.myProfile);
router.put('/user', controller.checkTan, controller.checkIsUser, controller.updateUser);
router.put('/password',	controller.checkIsUser, controller.updatePassword);
router.delete('/user', controller.checkIsUser, controller.deleteOwnUser);
router.get('/info-mail-apps', controller.queryInfoMailState);
router.get('/info', controller.checkIsUser, controller.queryInfo);

//tan process, used for securing critical operations
router.get('/tan-request', controller.startTanProcess);

//password Reset an self registration with captcha protection
router.get('/captcha', controller.createCaptcha);
router.post('/reset-request', controller.checkCaptcha, controller.startResetProcess);
router.post('/reset-confirm', controller.completeResetProcess);
router.post('/register-request', controller.checkCaptcha, controller.startRegisterProcess);
router.post('/register-confirm', controller.completeRegisterProcess);
router.get('/registration-state', controller.queryRegistrationState);

//api - admin
router.get('/admin/user', controller.checkAdmin, controller.checkTan, controller.getUserList);
router.put('/admin/user',	controller.checkAdmin,	controller.checkTan, controller.saveUser, controller.getUserList);
router.post('/admin/user',	controller.checkAdmin,	controller.checkTan, controller.addUser, controller.getUserList);
router.delete('/admin/user/:login', controller.checkAdmin,	controller.checkTan, controller.deleteUser, controller.getUserList);
router.post('/admin/message',	controller.checkAdmin, controller.checkTan,	controller.sendMessage);
router.get('/admin/version', controller.checkAdmin,	controller.queryPackages);
router.delete('/admin/token/:login', controller.checkAdmin, controller.deleteRefreshToken);
// api - admin - templates
router.get('/admin/templates', controller.checkAdmin, controllerTemplates.getList);
router.get('/admin/templates/:id', controller.checkAdmin, controllerTemplates.get);
router.delete('/admin/templates/:id', controller.checkAdmin, controllerTemplates.delete);
router.put('/admin/templates/:id', controller.checkAdmin, controllerTemplates.update);
router.post('/admin/templates', controller.checkAdmin, controllerTemplates.create);
router.get('/admin/templatesAvailable', controller.checkAdmin, controllerTemplates.getListAvailable);
// Regular templates
router.get('/templatesByTypeLang/:tplId/:lang', controllerTemplates.getByTypeLang);
module.exports=router;
