const CHARS="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

/*
Generate random alphanum string - used for tickets or initial passwords
	options.length: number of characters
	options.numbers: flag if numbers should be used (aside to small and capital letters)
*/
exports.generate=function(options) {
	//set default options, if options or length missing
	if (!options || !options.length) options={ length:20, numbers:true};	
	var result='';
	for (var i=0; i<options.length; i++) {
		var index;
		if (options.numbers) index=Math.floor(Math.random()*CHARS.length);
		else index=Math.floor(Math.random()*(CHARS.length-10));
		result+=CHARS.charAt(index);
	}
	return result;
}
