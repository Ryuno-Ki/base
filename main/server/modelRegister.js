var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var UserDataSchema=new Schema({
	login:		{type: String, required: true, max: 100},
	password:	{type: String, required: true},
	name:		{type: String, max: 100},
	email:		{type: String, max: 100},
	comment:	{type: String, default: "", max: 1000},
	lang:		{type: String, max:10, default:"de"}
});
var RegisterSchema=new Schema({
	user:		UserDataSchema,
	ticket:		{type: String, unique:true, required: true},
	created:	{type: Date, required: true}
});


module.exports=mongoose.model('Register', RegisterSchema );
