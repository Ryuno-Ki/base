export const server = {
    moduleId: 'main',
    templates: [
        'mails::register::admin',
        'mails::register::user', 
        'mails::tan::request',
        'mails::user::deleted::admin',
        'mails::user::deleted',
        'mails::user::expire',
        'mails::user::limit::hard',
        'mails::user::limit::soft',
        'mails::user::pwreset',
        'mails::user::welcome',
        'pages::imprint',
        'pages::privacy',
        'pages::terms'
    ]
}